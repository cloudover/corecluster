from distutils.core import setup
from setuptools import find_packages


setup(
  name='corecluster',
  packages=find_packages(exclude=['config', 'config.*']),
  version='18.03.01',
  description='CloudOver core IaaS system',
  author='Marta Nabozny',
  author_email='marta.nabozny@cloudover.io',
  url='http://cloudover.org/corecluster/',
  download_url='https://github.com/cloudOver/CoreCluster/archive/master.zip',
  keywords=['corecluster', 'cloudover', 'cloud'],
  classifiers=[],
  install_requires=['corenetwork', 'redis'],
  include_package_data=True,
)
