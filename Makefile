all:
	mkdir -p $(DESTDIR)/etc/corecluster/
	mkdir -p $(DESTDIR)/etc/rsyslog.d/
	mkdir -p $(DESTDIR)/etc/avahi/services/
	mkdir -p $(DESTDIR)/etc/uwsgi/apps-enabled/
	mkdir -p $(DESTDIR)/etc/nginx/sites-enabled/
	mkdir -p $(DESTDIR)/usr/sbin/
	mkdir -p $(DESTDIR)/var/lib/cloudOver/storages
	mkdir -p $(DESTDIR)/var/lib/cloudOver/sheepdog
	mkdir -p $(DESTDIR)/etc/cron.d/
	mkdir -p $(DESTDIR)/lib/systemd/system/
	python3 setup.py install --root=$(DESTDIR)
	cp -r config/* $(DESTDIR)/etc/corecluster/
	cp -r sbin/* $(DESTDIR)/usr/sbin/
	cp corecluster.cron $(DESTDIR)/etc/cron.d/corecluster
	cp corecluster.service $(DESTDIR)/lib/systemd/system/
	echo -n "version=\"$(VERSION)\"" > $(DESTDIR)/etc/corecluster/version.py
	cp config/rsyslog.conf $(DESTDIR)/etc/rsyslog.d/20-corecluster.conf
	cp config/avahi-services/corecluster-api.service $(DESTDIR)/etc/avahi/services/corecluster-api.service
	cp config/avahi-services/corecluster-ci.service $(DESTDIR)/etc/avahi/services/corecluster-ci.service
	ln -s /etc/corecluster/uwsgi/corecluster-api.ini $(DESTDIR)/etc/uwsgi/apps-enabled/corecluster-api.ini
	ln -s /etc/corecluster/uwsgi/corecluster-ci.ini $(DESTDIR)/etc/uwsgi/apps-enabled/corecluster-ci.ini
	ln -s /etc/corecluster/nginx/corecluster-api $(DESTDIR)/etc/nginx/sites-enabled/corecluster-api
	ln -s /etc/corecluster/nginx/corecluster-ci $(DESTDIR)/etc/nginx/sites-enabled/corecluster-ci
