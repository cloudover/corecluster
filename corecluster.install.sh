#!/bin/bash
echo "coreCluster: Installing PyPi packages"
pip3 install -U redis

echo "coreCluster: Changing permissions"
chown -R syslog:adm /var/log/cloudOver 2>/dev/null || true
chown -R syslog:cloudover /var/log/cloudOver 2> /dev/null || true
chown -R cloudover:cloudover /var/log/cloudOver 2> /dev/null || true
chmod -R ug+rw /var/log/cloudOver 2> /dev/null || true

echo "coreCluster: Restarting rsyslog service"
service rsyslog restart || service rsyslogd restart || true

if ! [ -f /etc/corecluster/config.py ] ; then
    echo "coreCluster: Creating default main configuration file"
    SECRET=`head -c 100 /dev/urandom | md5sum | cut -f 1 -d ' '`
    INSTALLATION_ID=`head -c 100 /dev/urandom | md5sum | cut -f 1 -d ' '`
    sed -e "s/OC_SECRET_KEY/$SECRET/"\
        -e "s/RANDOM_INSTALLATION_ID/$INSTALLATION_ID/"\
        /etc/corecluster/config.example > /etc/corecluster/config.py
fi

if ! [ -f /etc/corecluster/agent.py ] ; then
    echo "coreCluster: Creating default configuration file for agents"
    cp /etc/corecluster/agent.example /etc/corecluster/agent.py
fi

echo "coreCluster: Migrating databases"
/usr/sbin/cc-admin migrate --noinput
/usr/sbin/cc-admin migrate --database=logs --noinput

echo "coreCluster: Changing permissions"
chown -R cloudover:cloudover /var/lib/cloudOver || true
chown -R cloudover:cloudover /usr/lib/cloudOver/storages || true
chown -R cloudover:www-data /etc/corecluster
chown -R cloudover:www-data /etc/corenetwork
chmod 660 /etc/corecluster/config.py
chmod 660 /etc/corecluster/agent.py
chmod 660 /etc/corecluster/version.py
chmod 660 /etc/corenetwork/config.py

echo "coreCluster: Collecting static files"
chown -R cloudover:www-data /var/lib/cloudOver/static/ 2> /dev/null|| true
/usr/sbin/cc-admin collectstatic --noinput || true

find /etc/corecluster -name "*.pyc" -exec rm + 2> /dev/null || true
find /etc/corenetwork -name "*.pyc" -exec rm + 2> /dev/null || true
find /usr/local/lib/python2.7/dist-packages/corecluster -name "*.pyc" -exec rm + 2> /dev/null || true
find /usr/local/lib/python2.7/dist-packages/corenetwork -name "*.pyc" -exec rm + 2> /dev/null || true

echo "coreCluster: Restarting avahi service"
service avahi-daemon restart

echo "coreCluster: Restarting uwsgi"
systemctl restart uwsgi

echo "coreCluster: Restarting Nginx"
service nginx restart
systemctl restart nginx

echo "coreCluster: Enabling corecluster service"
systemctl enable corecluster
systemctl restart corecluster
