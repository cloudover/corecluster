# Generated by Django 2.0.4 on 2018-04-14 20:01

import corecluster.models.common_models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('corecluster', '0052_rename_user_fields'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='first_name',
            new_name='fist_name',
        ),
        migrations.RemoveField(
            model_name='image',
            name='token',
        ),
        migrations.AlterField(
            model_name='agent',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='agent',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='agent',
            name='ssh_public_key',
            field=models.CharField(default='', max_length=1024),
        ),
        migrations.AlterField(
            model_name='clusterid',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='clusterid',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='device',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='device',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='group',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='group',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='group',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='group',
            name='quota_storage',
            field=models.BigIntegerField(default=0, help_text='Storage quota in bytes'),
        ),
        migrations.AlterField(
            model_name='group',
            name='role',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corecluster.Role'),
        ),
        migrations.AlterField(
            model_name='image',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='image',
            name='access',
            field=models.CharField(choices=[('private', 'private'), ('public', 'public'), ('group', 'group')], default='private', max_length=30),
        ),
        migrations.AlterField(
            model_name='image',
            name='disk_controller',
            field=models.CharField(choices=[('ide', 'ide'), ('sata', 'sata'), ('scsi', 'scsi'), ('virtio', 'virtio')], default='virtio', max_length=10),
        ),
        migrations.AlterField(
            model_name='image',
            name='format',
            field=models.CharField(choices=[('qcow2', 'qcow2'), ('qcow', 'qcow'), ('raw', 'raw'), ('vdi', 'vdi')], default='qcow2', max_length=10),
        ),
        migrations.AlterField(
            model_name='image',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.Group'),
        ),
        migrations.AlterField(
            model_name='image',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='image',
            name='network_device',
            field=models.CharField(choices=[('e1000', 'e1000'), ('i82551', 'i82551'), ('i82557b', 'i82557b'), ('i82559er', 'i82559er'), ('ne2k_isa', 'ne2k_isa'), ('ne2k_pci', 'ne2k_pci'), ('pcnet', 'pcnet'), ('rtl8139', 'rtl8139'), ('virtio', 'virtio')], default='e1000', max_length=10),
        ),
        migrations.AlterField(
            model_name='image',
            name='size',
            field=models.BigIntegerField(blank=True, help_text='Image file size in bytes', null=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='type',
            field=models.CharField(choices=[('transient', 'transient'), ('permanent', 'permanent'), ('cd', 'cd'), ('object', 'object')], default='object', max_length=20),
        ),
        migrations.AlterField(
            model_name='image',
            name='video_device',
            field=models.CharField(choices=[('cirrus', 'cirrus'), ('qxl', 'qxl'), ('xen', 'xen'), ('vbox', 'vbox'), ('vga', 'vga'), ('vmvga', 'vmvga')], default='qxl', max_length=10),
        ),
        migrations.AlterField(
            model_name='lease',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='lease',
            name='access',
            field=models.CharField(choices=[('private', 'private'), ('public', 'public'), ('group', 'group')], default='private', max_length=30),
        ),
        migrations.AlterField(
            model_name='lease',
            name='gateway_of',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='gateway_set', to='corecluster.Subnet'),
        ),
        migrations.AlterField(
            model_name='lease',
            name='hostname',
            field=models.CharField(default='vm', max_length=200),
        ),
        migrations.AlterField(
            model_name='lease',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='access',
            field=models.CharField(choices=[('private', 'private'), ('public', 'public'), ('group', 'group')], default='private', max_length=30),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.Group'),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='mask',
            field=models.PositiveIntegerField(help_text='Network mask in short format (e.g. 24 for 255.255.255.0 network)'),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='mode',
            field=models.CharField(choices=[('routed', 'routed'), ('public', 'public'), ('isolated', 'isolated')], default='public', help_text='Network mode.', max_length=40),
        ),
        migrations.AlterField(
            model_name='networkpool',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.User'),
        ),
        migrations.AlterField(
            model_name='node',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='node',
            name='address',
            field=models.GenericIPAddressField(help_text='Node ip address'),
        ),
        migrations.AlterField(
            model_name='node',
            name='auth_token',
            field=models.TextField(blank=True, default='', help_text='Authentication string used by node to authenticate itself', null=True),
        ),
        migrations.AlterField(
            model_name='node',
            name='cpu_total',
            field=models.IntegerField(help_text='CPUs available for cloud'),
        ),
        migrations.AlterField(
            model_name='node',
            name='driver',
            field=models.CharField(default='qemu', help_text='Virtualisation driver for libvirt. Used also as template name for VM definition', max_length=45),
        ),
        migrations.AlterField(
            model_name='node',
            name='hdd_total',
            field=models.IntegerField(help_text="Node's disk capacity in MB"),
        ),
        migrations.AlterField(
            model_name='node',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='node',
            name='installation_id',
            field=models.TextField(blank=True, default='', help_text="Node's installation id. Used by node to authenticate itself", null=True),
        ),
        migrations.AlterField(
            model_name='node',
            name='mac',
            field=models.CharField(blank=True, default='', help_text='Mac address for node wakeup', max_length=30),
        ),
        migrations.AlterField(
            model_name='node',
            name='memory_total',
            field=models.IntegerField(help_text="Node's memory available for cloud in MB"),
        ),
        migrations.AlterField(
            model_name='node',
            name='suffix',
            field=models.CharField(default='/system', help_text='Libvirt connection url suffix. /system for qemu', max_length=20),
        ),
        migrations.AlterField(
            model_name='node',
            name='transport',
            field=models.CharField(default='ssh', help_text='Network transport for libvirt', max_length=45),
        ),
        migrations.AlterField(
            model_name='node',
            name='username',
            field=models.CharField(help_text='User account which will be used to connect with this node', max_length=30),
        ),
        migrations.AlterField(
            model_name='permission',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='execution_time',
            field=models.IntegerField(default=0, help_text='Total time in milliseconds spent on function execution time'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='role',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='role',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='role',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='storage',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='storage',
            name='capacity',
            field=models.BigIntegerField(help_text='Total capacity in MB'),
        ),
        migrations.AlterField(
            model_name='storage',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='storage',
            name='transport',
            field=models.CharField(default='netfs', max_length=20),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='access',
            field=models.CharField(choices=[('private', 'private'), ('public', 'public'), ('group', 'group')], default='private', max_length=30),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.User'),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='v_id',
            field=models.PositiveIntegerField(blank=True, default=None, help_text='VLAN or VxLAN id. Used only with isolated network pools', null=True),
        ),
        migrations.AlterField(
            model_name='template',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='template',
            name='cmdline',
            field=models.CharField(blank=True, default='', help_text='Kernel command line parameters. If using default hvm type, leave it blank', max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='template',
            name='domain_type',
            field=models.CharField(default='hvm', max_length=64),
        ),
        migrations.AlterField(
            model_name='template',
            name='ec2name',
            field=models.CharField(default='t1.micro', help_text='Amazon EC2 template equivalent', max_length=40),
        ),
        migrations.AlterField(
            model_name='template',
            name='hdd',
            field=models.IntegerField(help_text='Maximum base image size in megabytes'),
        ),
        migrations.AlterField(
            model_name='template',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='template',
            name='initrd',
            field=models.CharField(blank=True, default='', help_text='Path to initrd used for booting virtual machine. If using default hvm type, leave it blank', max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='template',
            name='kernel',
            field=models.CharField(blank=True, default='', help_text='Path to kernel used for booting virtual machine. If using default hvm type, leave it blank', max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='template',
            name='memory',
            field=models.IntegerField(help_text='Memory in megabytes'),
        ),
        migrations.AlterField(
            model_name='token',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='token',
            name='access',
            field=models.CharField(choices=[('private', 'private'), ('public', 'public'), ('group', 'group')], default='private', max_length=30),
        ),
        migrations.AlterField(
            model_name='token',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.Group'),
        ),
        migrations.AlterField(
            model_name='token',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='token',
            name='name',
            field=models.CharField(default='', max_length=256),
        ),
        migrations.AlterField(
            model_name='user',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='user',
            name='group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corecluster.Group'),
        ),
        migrations.AlterField(
            model_name='user',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corecluster.Role'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='_data',
            field=models.TextField(default='{}', help_text='Additional data. Leave blank if not used'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='access',
            field=models.CharField(choices=[('private', 'private'), ('public', 'public'), ('group', 'group')], default='private', max_length=30),
        ),
        migrations.AlterField(
            model_name='vm',
            name='base_image',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.Image'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='vm',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.Group'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='id',
            field=models.CharField(default=corecluster.models.common_models.id_generator, help_text='API id used to identify all objects in Core', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='vm',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corecluster.Node'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='template',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corecluster.Template'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='corecluster.User'),
        ),
        migrations.AlterField(
            model_name='vm',
            name='vnc_address',
            field=models.CharField(default='', max_length=128, null=True),
        ),
    ]