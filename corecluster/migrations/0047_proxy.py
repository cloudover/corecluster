# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-04-27 12:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corecluster', '0046_migrate_hash'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vm',
            name='websocket_enabled',
        ),
        migrations.RemoveField(
            model_name='vm',
            name='websocket_port',
        ),
        migrations.AddField(
            model_name='lease',
            name='proxy_enabled',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='lease',
            name='proxy_port',
            field=models.IntegerField(default=80),
        ),
    ]
