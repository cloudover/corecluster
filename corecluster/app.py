MODULE = {
    'models': ['corecluster.models.core'],
    'api': {
        'api/api': 'corecluster.views.api.api',
        'api/image': 'corecluster.views.api.image',
        'api/lease': 'corecluster.views.api.lease',
        'api/network': 'corecluster.views.api.network',
        'api/proxy': 'corecluster.views.api.proxy',
        'api/redirection': 'corecluster.views.api.redirection',
        'api/storage': 'corecluster.views.api.storage',
        'api/task': 'corecluster.views.api.task',
        'api/template': 'corecluster.views.api.template',
        'api/vm': 'corecluster.views.api.vm',
        'user/account': 'corecluster.views.user.account',
        'user/permission': 'corecluster.views.user.permission',
        'user/token': 'corecluster.views.user.token',
        'user/vm': 'corecluster.views.user.user',
    },
    'ci': {
        'ci/corosync': 'corecluster.views.ci.corosync',
        'ci/network_isolated': 'corecluster.views.ci.network_isolated',
        'ci/network_routed': 'corecluster.views.ci.network_routed',
        'ci/node': 'corecluster.views.ci.node',
        'ci/vm': 'corecluster.views.ci.vm',
    },
    'configs': {
        'core': '/etc/corecluster/config.py',
        'hardware': '/etc/corecluster/hardware.py',
        'agent': '/etc/corecluster/agent.py',
    },
    'hooks': {
        'agent.vm.create': ['corecluster.hooks.vm'],
        'agent.vm.remove_vm': ['corecluster.hooks.vm'],
        'cron.minute': ['corecluster.hooks.node_libvirt', 'corecluster.hooks.wakeonlan'],
        'cron.hourly': ['corecluster.hooks.vm_cleanup_db', 'corecluster.hooks.vm_cleanup_task'],
        'cron.daily': [],
    },
    'agents': [
        {'type': 'vm', 'module': 'corecluster.agents.vm', 'count': 4},
        {'type': 'network', 'module': 'corecluster.agents.network', 'count': 4},
        {'type': 'console', 'module': 'corecluster.agents.console', 'count': 1}
    ],
    'drivers': {

    },
    'algorithms': {

    },
    'cli': {
        'agent': 'corecluster.cli.agent',
        'api': 'corecluster.cli.api',
        'image': 'corecluster.cli.image',
        'network_pool': 'corecluster.cli.network_pool',
        'node': 'corecluster.cli.node',
        'storage': 'corecluster.cli.storage',
        'subnet': 'corecluster.cli.subnet',
        'task': 'corecluster.cli.task',
        'vm': 'corecluster.cli.vm',
    },
    'coremetry': [
        'corecluster.coremetry.node',
        'corecluster.coremetry.storage',
        'corecluster.coremetry.system',
        'corecluster.coremetry.vm',
    ]
}
