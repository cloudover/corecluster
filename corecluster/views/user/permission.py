"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from corecluster.models.core.token import Token
from corecluster.models.core import Permission
from corecluster.utils.exception import CoreException
import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui


class Api(ApiInterface):
    model = Permission
    auth = 'password'
    log = 'all'
    expose_methods = [
        'get_list',
        'attach',
        'detach',
    ]

    failed_seeds = {}

    @validate(token_id=v.is_id(), function=v.is_string())
    @render_as_ui
    def attach(self, context, token_id, function):
        """
        Attach permission to token. If token has no permissions, it enables checking permissions for given token.
        :param token_id: Token id
        :param function: function name (e.g. api/image/get_list) without initial and trailing slash (/)
        """
        token = Token.get(context.user_id, token_id)
        try:
            permission = Permission.objects.get(function=function)
            token.permissions.add(permission)
            token.ignore_permissions = False
            token.save()
        except Exception as e:
            raise CoreException('function_not_found')

    @validate(token_id=v.is_id(), function=v.is_string())
    @render_as_ui
    def detach(self, context, token_id, function):
        """
        Remove permission from token
        :param token_id: Token id
        :param function: function name (e.g. api/image/get_list) without initial and trailing slash (/)
        """
        try:
            token = Token.get(context.user_id, token_id)
            permission = token.permissions.get(function=function)
            token.permissions.remove(permission)
            token.ignore_permissions = False
            token.save()
        except Exception as e:
            raise CoreException('function_not_found')
