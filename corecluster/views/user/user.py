"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import time
import random
import hashlib
import datetime

from corecluster.models.core.user import User
from corecluster.utils.exception import CoreException
import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui


class Api(ApiInterface):
    model = User
    auth = 'password'
    log = 'all'
    expose_methods = [
        'register',
        'create',
        'delete',
    ]

    failed_seeds = {}

    @validate(password_hash=v.is_string(), password_seed=v.is_string())
    @render_as_ui
    def change_password(self, context, password_hash, password_seed):
        """
        Change user's password to new. Use sha1 of concatenated passford and seed to generate hash. The seed should be at
        least 10 characters long.
        :param password_hash: Hash generated from user's password and password_seed. To generate hash use sha1(passwd+seed)
        :param password_seed: Random seed
        """
        if len(password_seed) < 10:
            raise CoreException('seed_too_short')

        context.user.pw_hash = password_hash
        context.user.pw_seed = password_seed
        context.user.save()

    @render_as_ui
    def get_detailsd(self, context):
        """
        Get information about account (name, surname, email etc.)
        """
        return context.user.to_dict


    @render_as_ui
    def get_quota(self, context):
        """
        Get information about quota (cpu, memory, storage)
        """
        return context.user.get_quota()
