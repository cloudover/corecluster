"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import time
import random
import hashlib
from corecluster.models.core.user import User
from corecluster.utils.exception import CoreException
import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui


class Api(ApiInterface):
    model = User
    auth = 'guest'
    log = 'all'
    expose_methods = [
        'register',
        'get_seed',
    ]

    failed_seeds = {}

    @validate(login=v.is_string(), password=v.is_string(), first_name=v.is_string(), last_name=v.is_string())
    @render_as_ui
    def register(self, context, password, first_name, last_name, email):
        """
        Register new user account with given credentials.
        :param password: Password
        :param name: First name
        :param surname: Last name
        :param email: User's email
        :return: Dictionary with user's login and password seed
        """
        if User.objects.filter(email=email).count() > 0:
            raise CoreException('user_exists')

        user = User.create()
        user.first_name = first_name
        user.last_name = last_name
        user.email = email
        user.pw_seed = hashlib.sha1(str(random.random())).hexdigest()[:10]
        user.make_pwd(password)
        user.state = 'ok'
        user.save()
        try:
            user.save()
        except Exception as e:
            context.log.debug(0, "User.register: Adding to DB: %s" % str(e))
            raise CoreException('user_create')

        return {'email': user.email, 'pw_seed': user.pw_seed}

    @validate(login=v.is_string())
    def get_seed(self, context, email):
        """
        Get seed to generate password hash for given login
        :param login: User's login
        :return: Seed for user's password hash
        """
        time.sleep(random.random() * 3)

        try:
            user = User.objects.get(email=email)
            return {'seed': user.pw_seed}
        except Exception as e:
            # Prevent guessing login names. Without this attacker could guess which logins are valid and which are not.
            if email not in self.failed_seeds:
                self.failed_seeds[email] = hashlib.sha1(str(random.random())).hexdigest()[:10]

            return {'seed': self.failed_seeds[email]}

