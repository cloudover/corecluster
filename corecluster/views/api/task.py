"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui
from corecluster.utils.exception import CoreException
from corecluster.cache.task import Task
from corenetwork.utils import config
import time


class Api(ApiInterface):
    model = Task
    auth = 'token'
    log = 'all'

    expose_methods = [
        'describe',
        'cancel',
        'wait',
    ]

    @render_as_ui
    @validate(id=v.is_id())
    def cancel(self, context, id):
        """
        Cancel tasks by id
        """
        task = Task(cache_key=id)
        if task.user_id == context.user_id:
            task.set_state('canceled')
            task.save()

    @render_as_ui
    @validate(id=v.is_id(), state=v.is_string(), timeout=v.is_integer(none=True))
    def wait(self, context, id, state, timeout=1000):
        '''
        Wail for task is done
        :param context:
        :param task_id:
        :param state:
        :param timeout:
        :return:
        '''
        for i in range(int(timeout/config.get('agent', 'TASK_FETCH_INTERVAL', 20))):
            task = Task(cache_key=id)

            if not hasattr(task, 'user_id') or task.user_id != context.user_id:
                raise CoreException('not_owner')

            if task.state == state:
                return
            else:
                time.sleep(config.get('agent', 'TASK_FETCH_INTERVAL', 20))
        raise CoreException('timeout')

