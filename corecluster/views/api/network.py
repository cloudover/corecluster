"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from corecluster.utils.exception import CoreException
from corecluster.models.core.network_pool import NetworkPool
from corecluster.models.core.subnet import Subnet
import corecluster.utils.validation as v
from corecluster.utils.api_interface import ApiInterface, validate, render_as_ui


class Api(ApiInterface):
    model = Subnet
    auth = 'token'
    log = 'all'
    expose_methods = [
        'get_list',
        'get_by_id',
        'edit',
        'describe',
        'create',
        'allocate',
        'delete',
        'get_pool_list',
    ]

    @render_as_ui
    @validate(address=v.is_string(),
              mask=v.is_integer(),
              name=v.is_string(),
              mode=v.is_string(),
              isolated=v.is_integer(none=True))
    def create(self, context, address, mask, name, mode, isolated=False):
        """
        Request new network
        :param address: network address or none
        :param mask: network mask
        :param name: network's name
        :param isolated: should network be isolated from any other traffic (including the Internet)
        :param mode: Type of network (routed - network is routed; isolated - vlan based or vpn; public - public ip set)
        :return: dictionary with new network
        """

        context.user.check_lease_quota(mode, mask)
        context.user.check_network_quota(mode, 1)

        if mask < 0 or mask >= 32:
            raise CoreException('network_invalid_mask')

        if mode == 'routed':
            net = Subnet.create_routed(context, None, mask, name, isolated)
        elif mode == 'public':
            net = Subnet.create_public(context, None, mask, name, isolated)
        elif mode == 'isolated':
            net = Subnet.create_isolated(context, address, mask, name)
        else:
            raise CoreException('network_mode_not_supported')

        net.prepare()
        return net.to_dict

    @render_as_ui
    @validate(id=v.is_id())
    def allocate(self, context, id):
        """
        Populate routed or public network with leases
        """
        network = Subnet.get(context.user_id, id)
        network.allocate()

    @render_as_ui
    @validate(id=v.is_id())
    def delete(self, context, id):
        """
        Remove network
        """
        subnet = Subnet.get(context.user_id, id)

        if subnet.is_in_use():
            raise CoreException('network_in_use')

        subnet.release()

    @render_as_ui
    def get_pool_list(self, context):
        """
        List all network pools, which are allowed to create new network inside
        """
        network_pools = NetworkPool.get_list(context.user_id, criteria={'state': 'ok'})
        response = []
        for network in network_pools:
            response.append(network.to_dict)
        return response
