"""
Copyright (C) 2014-2017 cloudover.io ltd.
This file is part of the CloudOver.org project

Licensee holding a valid commercial license for this software may
use it in accordance with the terms of the license agreement
between cloudover.io ltd. and the licensee.

Alternatively you may use this software under following terms of
GNU Affero GPL v3 license:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. For details contact
with the cloudover.io company: https://cloudover.io/


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.


You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from corecluster.models.core.template import Template
from corecluster.utils import validation as v
from corecluster.utils.api_interface import ApiInterface, render_as_ui



class Api(ApiInterface):
    model = Template
    auth = 'token'
    log = 'all'

    expose_methods = [
        'get_list',
        'get_by_id',
        'describe',
        'capacity',
    ]

    @render_as_ui
    def capacity(self, context):
        """
        :return: List with cluster's capacity for each template
        """
        from corecluster.models.core import Node

        t = {}
        for template in Template.objects.filter(state='active'):
            t[template.id] = 0
            for node in Node.objects.filter(state='ok'):
                available_cpu = int(node.cpu_free / template.cpu)
                available_memory = int(node.memory_free / template.memory)
                t[template.id] += max(available_cpu, available_memory)
        return t
